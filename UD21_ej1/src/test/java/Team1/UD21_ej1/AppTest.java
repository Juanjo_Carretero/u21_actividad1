package Team1.UD21_ej1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import views.principal;


public class AppTest 
extends TestCase
{
	@Test
	public void testSuma() {
		double resultado = principal.operaciones(2.0,2.0,"+");
		double esperado = 4.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testResta() {
		double resultado = principal.operaciones(2.0,2.0,"-");
		double esperado = 0.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMulti() {
		double resultado = principal.operaciones(2.0,2.0,"*");
		double esperado = 4.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivi() {
		double resultado = principal.operaciones(8.0,2.0,"/");
		double esperado = 4.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRaiz() {
		double resultado = principal.operaciones(9.0,0.0,"raiz");
		double esperado = 3.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testElevado() {
		double resultado = principal.operaciones(2.0,2.0,"elevado");
		double esperado = 4.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testInverso() {
		double resultado = principal.operaciones(4.0,0.0,"inverso");
		double esperado = 4.0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testPorcentaje() {
		double resultado = principal.operaciones(9.0,2.0,"%");
		double esperado = 0.18;
		assertEquals(esperado, resultado);
	}
	
}
