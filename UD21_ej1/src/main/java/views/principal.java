package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class principal extends JFrame {

	private JPanel contentPane;
	private JTextField textField_resultado;

	private JButton porcentaje;
	private JButton ce;
	private JButton c;
	private JButton borrar;
	private JButton dividido;
	private JButton multiplicacion;
	private JButton resta;
	private JButton suma;
	private JButton igual;
	private JButton inverso;
	private JButton elevado;
	private JButton raiz;
	private JButton cero;
	private JButton uno;
	private JButton dos;
	private JButton tres;
	private JButton cuatro;
	private JButton cinco;
	private JButton seis;
	private JButton siete;
	private JButton ocho;
	private JButton nueve;
	private JButton masmenos;
	private JButton coma;
	private JLabel historialtag;
	private JTextArea textarea_historial;
	private boolean iteracion=false;
	private double operador1;
	private double operador2;
	private double calculo=0;
	private String aux="";
	private String numeropicado="";
	private String operacion;
	
	public principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		porcentaje = new JButton("%");
		porcentaje.setName("porciento");
		porcentaje.setBounds(10, 60, 46, 23);
		contentPane.add(porcentaje);
		porcentaje.addActionListener(conseguiroperacion);
		
		ce = new JButton("CE");
		ce.setBounds(66, 60, 56, 23);
		contentPane.add(ce);
		ce.addActionListener(eliminar);
		
		c = new JButton("C");
		c.setBounds(132, 60, 53, 23);
		contentPane.add(c);
		c.addActionListener(borrartodo);
		
		borrar = new JButton("Eliminar");
		borrar.setBounds(218, 60, 82, 23);
		contentPane.add(borrar);
		borrar.addActionListener(eliminar);
		
		dividido = new JButton("/");
		dividido.setName("division");
		dividido.setBounds(228, 94, 49, 23);
		contentPane.add(dividido);
		dividido.addActionListener(conseguiroperacion);
		
		multiplicacion = new JButton("*");
		multiplicacion.setName("multiplicacion");
		multiplicacion.setBounds(228, 128, 49, 23);
		contentPane.add(multiplicacion);
		multiplicacion.addActionListener(conseguiroperacion);
		
		resta = new JButton("-");
		resta.setName("resta");
		resta.setBounds(228, 162, 49, 23);
		contentPane.add(resta);
		resta.addActionListener(conseguiroperacion);
		
		suma = new JButton("+");
		suma.setName("suma");
		suma.setBounds(228, 196, 49, 23);
		contentPane.add(suma);
		suma.addActionListener(conseguiroperacion);
		
		igual = new JButton("=");
		igual.setBounds(228, 230, 49, 23);
		contentPane.add(igual);
		igual.addActionListener(resultado);
		
		inverso = new JButton("1/x");
		inverso.setName("inverso");
		inverso.setBounds(10, 94, 56, 23);
		contentPane.add(inverso);
		inverso.addActionListener(conseguiroperacion);
		
		elevado = new JButton("x2");
		elevado.setName("elevado");
		elevado.setBounds(76, 94, 56, 23);
		contentPane.add(elevado);
		elevado.addActionListener(conseguiroperacion);
		
		raiz = new JButton("raiz2");
		raiz.setName("raiz");
		raiz.setBounds(146, 94, 69, 23);
		contentPane.add(raiz);
		raiz.addActionListener(conseguiroperacion);
		
		siete = new JButton("7");
		siete.setBounds(10, 128, 56, 23);
		contentPane.add(siete);
		siete.addActionListener(añadir);
		
		ocho = new JButton("8");
		ocho.setBounds(76, 128, 56, 23);
		contentPane.add(ocho);
		ocho.addActionListener(añadir);
		
		nueve = new JButton("9");
		nueve.setBounds(149, 128, 56, 23);
		contentPane.add(nueve);
		nueve.addActionListener(añadir);
		
		cuatro = new JButton("4");
		cuatro.setBounds(10, 162, 56, 23);
		contentPane.add(cuatro);
		cuatro.addActionListener(añadir);
		
		cinco = new JButton("5");
		cinco.setBounds(76, 162, 56, 23);
		contentPane.add(cinco);
		cinco.addActionListener(añadir);
		
		seis = new JButton("6");
		seis.setBounds(148, 162, 57, 23);
		contentPane.add(seis);
		seis.addActionListener(añadir);
		
		tres = new JButton("3");
		tres.setBounds(148, 196, 57, 23);
		contentPane.add(tres);
		tres.addActionListener(añadir);
		
		dos = new JButton("2");
		dos.setBounds(76, 196, 56, 23);
		contentPane.add(dos);
		dos.addActionListener(añadir);
		
		uno = new JButton("1");
		uno.setBounds(10, 196, 56, 23);
		contentPane.add(uno);
		uno.addActionListener(añadir);
		
		masmenos = new JButton("+-");
		masmenos.setBounds(25, 230, 49, 23);
		contentPane.add(masmenos);
		
		cero = new JButton("0");
		cero.setBounds(86, 230, 53, 23);
		contentPane.add(cero);
		cero.addActionListener(añadir);
		
		coma = new JButton(",");
		coma.setBounds(148, 230, 39, 23);
		contentPane.add(coma);
		coma.addActionListener(añadir);
		
		textField_resultado = new JTextField();
		textField_resultado.setHorizontalAlignment(SwingConstants.CENTER);
		textField_resultado.setBounds(26, 24, 238, 20);
		contentPane.add(textField_resultado);
		textField_resultado.setColumns(10);
		
		historialtag = new JLabel("Historial");
		historialtag.setBounds(355, 27, 69, 14);
		contentPane.add(historialtag);
		
		textarea_historial = new JTextArea();
		textarea_historial.setBounds(310, 42, 124, 211);
		contentPane.add(textarea_historial);
	}
	
	ActionListener añadir = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			JButton v = (JButton) e.getSource();
			aux=aux+v.getText();
			
			numeropicado=v.getText();
			
			textField_resultado.setText(aux);
			
			if (!iteracion) {
				operador1 = Double.parseDouble(numeropicado);
			}else {
				operador2 = Double.parseDouble(numeropicado);
			}
		}
	};
	
	ActionListener borrartodo = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			textField_resultado.setText(null);
			
			iteracion=false;
			aux="";
		}
		
	};
	
	ActionListener conseguiroperacion = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			iteracion = true;
			
			JButton v = (JButton) e.getSource();
			aux=aux+v.getText();
			
			textField_resultado.setText(aux);
			
			String nombreoperacion = v.getName();
			
			switch (nombreoperacion) {
			case "suma":
				operacion = "+";
				break;
			case "resta":
				operacion = "-";
				break;
			case "multiplicacion":
				operacion = "*";
				break;
			case "division":
				operacion = "/";
				break;
			case "porciento":
				operacion = "%";
				break;
			case "raiz":
				operacion = "raiz";
				break;
			case "elevado":
				operacion = "elevado";
				break;
			case "inverso":
				operacion = "inverso";
				break;
			default:
				break;
			}
			
			
		}
	};
	
	ActionListener resultado = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			calculo = operaciones(operador1,operador2, operacion);
			
			String calculostring = String.valueOf(calculo);
			
			String operador1string = String.valueOf(operador1);
			
			String operador2string = String.valueOf(operador2);
			
			textField_resultado.setText(calculostring);
			
			if(!operacion.equals("raiz")&&!operacion.equals("inverso")) {
			textarea_historial.append(operador1string + " " + operacion + " " + operador2string + "=" + calculostring);
			textarea_historial.append(System.getProperty("line.separator")); 
			}else {
			textarea_historial.append(operador1string + " " + operacion + " " +"=" + calculostring);
			textarea_historial.append(System.getProperty("line.separator")); 
			}
				
			textField_resultado.setText(null);
			aux="";
			iteracion=false;
		}
	};
	
	ActionListener eliminar = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			textField_resultado.setText((textField_resultado.getText().substring(0,textField_resultado.getText().length()-1)));
		}
	};

	public static double operaciones(Double n1,Double n2, String operacion) {
		Double retorn=0.0;
		
		switch (operacion) {
		case "+":
			retorn = n1 + n2;
			break;
		
		case "-":
			retorn = n1 - n2;
			break;
		
		case "*":
			retorn = n1 * n2;
			break;
		
		case "/":
			retorn = n1 / n2;
			break;
		
		case "raiz":
			retorn = Math.sqrt(n1);
			break;
			
		case "elevado":
			retorn = Math.pow(n1, n2);
			break;
		case "inverso":
			retorn = n1 / 1;
			break;
		case "%":
			retorn = (n1 * n2) / 100;
			break;
		default:
			break;
		}
		
		return retorn;
	}
	
	
	
	
	
	
}

